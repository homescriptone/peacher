# Peacher
A organised FastAPI app.

## Architecture of the project :
```text
    - docs : Serve GitBook Documentation
    - src  : Fast API app folder
        -- mysql_db : MysqlClient Deps for Python 3.12
        -- peacher : Fast API app
            -- albic : Configuration folder of Alembic for SQLAlchemy ( env.py and migrations ),
            -- apps  : Core Folder of the app
                -- app_names.py : provide custom app folder to use for db migration.
                -- db.py : configure the sqlalchemy interface for the app,
                -- env.py : configure the .env to load for the app,
                -- init.py : override the venv source folder,
                -- run.py : load routers and create the app instance for fastapi
            -- command : Utilities to create quickly a fast api app
                -- create_app.py : similar to django startapp, he copies template from conf/app_template and
                create app with the name given
                -- abs_create_app.py : functions needed by create_app.py
                -- conf : template used by the command
            -- requirements : requirements to run this project
            -- templates : used by jinja to serve pages
            -- tests : write tests here
            alembic.ini : alembic config file to start the project
            manage.py   : entrypoint to start the project, migrate the database, create a app into the project.

```

# Usage :
Open the folder src and run following actions :

### Start FastAPI server
This start the uvicorn server on port 8000 in development mode.

```python
    cd src/peacher
    python manage.py start_server
```

### Migrate the databases
This migrate the models define in db.py
```python
    cd src/peacher
    python manage.py migrate_db
```

### Create a app
This create a app with the name given.
Provide informations related to the app.

```python
    cd src/peacher
    python manage.py create_app
```

## This is a demo project made to learn how to build a mini django project.