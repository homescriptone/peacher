from sqlalchemy import Column, Integer, DateTime, String, JSON, Table, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql.functions import now

from apps.db import Base, BaseMixin, session

followDetail = Table('followDetail',
                     Base.metadata,
                     Column('id', Integer, primary_key=True),
                     Column('followers', Integer, ForeignKey('Users.id')),
                     Column('following', Integer, ForeignKey('Followers.id')),
                     )


class Users(Base, BaseMixin):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(255), unique=True, nullable=True)
    email = Column(String(255), unique=True)
    full_name = Column(String(500), comment="Noms & Prenoms")
    sex = Column(String(500), comment="Sexe")

    def __init__(self, **kw):
        super(Users, self).__init__(**kw)
        self.create_connection_instance()

class Followers(Base, BaseMixin):
    __tablename__ = 'followers'

    id = Column(Integer, primary_key=True)
    follow_date = Column(DateTime, default=now())
    author = relationship('users', secondary=followDetail, backref='followers')

    def __init__(self, **kw):
        super(Followers, self).__init__(**kw)
        self.create_connection_instance()
