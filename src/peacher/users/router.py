from fastapi import APIRouter
from users.db import Users

router = APIRouter()
users = Users()


@router.get("/")
def get():
    """
      Get all users
    """
    return users.get_all()


@router.get("/{id}")
def get_by_id(id: int):
    """
      Get user by Id.
    """
    return users.get_by_id(id)


@router.put("/{id}")
def update_by_id(id: int):
    """
    Get a specific ressource.
  :param id: users Id. : WIP
  """
    return {}


@router.post("/")
def post():
    """
    Publish a new item. : WIP
  """
    return {}


@router.delete("/{id}")
def delete(id: int):
    """
    Delete a item. : WIP
  """
    return {}
