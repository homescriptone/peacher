import os

from dotenv import load_dotenv, find_dotenv
from sqlalchemy import URL


def load_env():
    # load the default config
    load_dotenv(find_dotenv())
    # load extra production config
    load_dotenv(find_dotenv(filename="prod.env"))


def get_db_url():
    # read .env config files.
    load_env()
    # url of engine db.
    url = URL.create(
        drivername=os.environ.get('DB_TYPE'),
        username=os.environ.get('DB_USERNAME'),
        password=os.environ.get('DB_PASSWORD'),
        host=os.environ.get('DB_HOST'),
        database=os.environ.get('DB_NAME'),
        port=os.environ.get('DB_PORT')
    )
    return url




if __name__ == "main":
    load_env()
