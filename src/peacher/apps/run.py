from fastapi import FastAPI
from users.router import router as user_router

app = FastAPI(
    title="Peacher",
    version="1.0",
    description="Peacher API",
    docs_url="/docs"
)

app.include_router(user_router, prefix="/users")

@app.get("/")
def root():
    return "Bienvenue sur Peacher"

