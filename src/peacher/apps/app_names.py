

def load_apps():
    """
        Add names to your custom apps here.
        This will be used by Alembic in env.py for migrations.
    """
    return [
        'users'
    ]