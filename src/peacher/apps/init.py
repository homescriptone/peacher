import sys
import os


def init():
    # Get the absolute path to the 'src/peacher' directory
    peacher_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

    # Add 'src/peacher' directory to the beginning of sys.path
    sys.path.insert(0, peacher_path)


if __name__ == "__main__":
    init()