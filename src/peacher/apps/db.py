from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, declarative_base
from apps.env import get_db_url

url = get_db_url()
engine = create_engine(url)
Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()

class BaseMixin(object):
    """
        Add a mixin to easily reuse the table for crud operations.
    """
    def __init__(self, **kw):
        super(BaseMixin, self).__init__(**kw)
        self.connection_instance = None

    def create_connection_instance(self):
        self.connection_instance = session.query(self.__class__)
        return self

    def get_all(self):
        return self.connection_instance.all()

    def get_by_id(self, id):
        return self.connection_instance.filter_by(id=id).first()