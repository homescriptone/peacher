import sys
import os
import argparse
from random import randint
from apps.env import load_env
import uvicorn


def init():
    # Get the absolute path to the 'src/peacher' directory
    peacher_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '.'))

    # Add 'src/peacher' directory to the beginning of sys.path
    sys.path.insert(0, peacher_path)


def main():
    usage = ("Shokays menu administration:\n"
             "\n"
             "- start_server : Start the shokays dev server\n"
             "- migrate_db   : Migrate data using sqlalchemy\n"
             "- create_app   : Create app based on a template\n"
             )
    parser = argparse.ArgumentParser(description="Shokays administration mecanism", usage=usage)
    parser.add_argument("action", choices=["start_server", "migrate_db", "create_app"], help=argparse.SUPPRESS)
    args = parser.parse_args()

    if args.action == "start_server":
        uvicorn.run("apps.run:app",
                    host="0.0.0.0",
                    port=8000,
                    reload=True,
                    log_level="debug",
                    workers=5,
                    limit_concurrency=10,
                    limit_max_requests=10
        )
    elif args.action == "migrate_db":
        from apps.db import url
        from alembic.config import Config
        from alembic import command
        # Create an Alembic config object
        config = Config(os.path.join(os.path.dirname(__file__), 'alembic.ini'))

        config.set_main_option('sqlalchemy.url', str(url))

        command.upgrade(config, "head")

        command.revision( config,
                          autogenerate=True,
                          message='Migrate DB v' + str(randint(0,500))

                          )

        command.upgrade(config, "head" )
    elif args.action == "create_app":
        from command.create_app import run
        app_name = input("Give your app_name : ")
        if app_name:
            run(app_name=app_name)


if __name__ == "__main__":
    load_env()
    init()
    main()
