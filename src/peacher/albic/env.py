from logging.config import fileConfig
import importlib
from sqlalchemy import engine_from_config
from sqlalchemy import pool
from alembic import context

from apps.db import Base
from apps.env import get_db_url
from apps.app_names import load_apps

# load custom models from apps.
loader_apps = load_apps()
for module_name in loader_apps:
    appname_model = module_name + str(".db")
    module = importlib.import_module(appname_model)

# base models.
target_metadata = Base.metadata

url = get_db_url()


# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = engine_from_config(
        url=url,
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
        configuration= config.get_section(config.config_ini_section, {})
    )



    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()