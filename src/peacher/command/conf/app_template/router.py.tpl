from fastapi import APIRouter

router = APIRouter()
@router.get("/")
def get():
  """
    Get ressources.
  """
  return [{}]

@router.get("/{id}")
def get_by_id(id:int):
  """
    Get a specific ressource.
  :param id: {app_name} Id.
  """
  return {}


@router.put("/{id}")
def update_by_id(id:int):
  """
    Get a specific ressource.
  :param id: {app_name} Id.
  """
  return {}


@router.post("/")
def post():
  """
    Publish a new item.
  """
  return {}


@router.delete("/{id}")
def delete(id:int):
  """
    Delete a item.
  """
  return {}

