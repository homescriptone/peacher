from sqlalchemy import Column, Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.functions import now

Base = declarative_base()

class {app_name}(Base):
  __tablename__ = '{app_name}'

  id = Column(Integer, primary_key=True)
  created_at = Column(DateTime, default=now())