import argparse

from command.abs_create_app import create_app


def run(app_name=None):
    parser = argparse.ArgumentParser(description="Create a FastAPI app architecture")

    if app_name is not None:
        create_app(app_name)
    else:
        parser.add_argument('app_name', help="app name")

        arguments = parser.parse_args()

        create_app(arguments.app_name)


if __name__ == '__main__':
    run()
