import os
import shutil


def copy_template(template_dir, destination_dir, project_name):
    for item in os.listdir(template_dir):
        src_item = os.path.join(template_dir, item)
        dest_item = os.path.join(destination_dir, item)

        if os.path.isdir(src_item):
            os.makedirs(dest_item, exist_ok=True)
            copy_template(src_item, dest_item, project_name)
        else:
            # If the file has a .tpl extension, remove it
            if item.endswith('.tpl'):
                dest_item = os.path.join(destination_dir, item[:-4])

            # Copy the file
            shutil.copy(src_item, dest_item)

            # Replace placeholders with the project name
            with open(dest_item, 'r') as f:
                file_content = f.read()

            file_content = file_content.replace('{app_name}', project_name)

            # Write the modified content back to the file
            with open(dest_item, 'w') as f:
                f.write(file_content)


def create_app(project_name):
    # Define the template directory
    template_dir = os.path.join(os.path.dirname(__file__), "conf/app_template")

    # Define the destination directory
    destination_dir = project_name

    # Create the destination directory
    os.makedirs(destination_dir, exist_ok=True)

    # Copy template files and directories recursively
    copy_template(template_dir, destination_dir, project_name)

    print(f"Project '{project_name}' created successfully!")
